

``` shell
### Pour télécharger les dépendances
terraform init
```


``` shell
### Pour télécharger les dépendances
terraform fmt
```


``` shell
### Pour valider la conf
terraform validate
```


Doc en cours :
https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started