data "template_file" "ansible" {
  template = "${file("./templates/hosts.tpl")}"
 
  vars = {
    connection_string_vm = "${join("\n", formatlist("%s ansible_host=%s env_name=%s ansible_ssh_user=root", digitalocean_droplet.vm.*.name ,  digitalocean_droplet.vm.*.ipv4_address, digitalocean_droplet.vm.*.name))}"
  }
}
 
resource "local_file" "ansible" {
  content  = "${data.template_file.ansible.rendered}"
  filename = "../../Formations/ansible/ansible/inventories/ubuntu.ini"
}


resource "null_resource" "web" {

  depends_on = ["digitalocean_droplet.vm", "local_file.ansible"]

  provisioner "local-exec" {
    working_dir = "/home/clamaud/Documents/Formations/ansible/ansible"
    command = "sleep 30 && ansible-playbook playbooks/test.yml -i inventories/ubuntu.ini"
  }
}
