variable "REGION" {
  default = "fra1"
}

variable "VM_IMAGE" {
  default = "ubuntu-18-04-x64"
}

variable "VM_SIZE" {
  default = "8gb"
}

variable "VM_SSH_KEYS" {
  default = "99:b0:23:5a:71:11:9c:56:70:4e:ec:ec:7c:c0:77:ef"
}

variable "STUDENT_NAME" {
  default = "studentX"
}

variable "VM_NUMBER" {
  default = 1
}